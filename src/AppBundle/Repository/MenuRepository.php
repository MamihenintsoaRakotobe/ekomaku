<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query;

/**
 * MenuRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MenuRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @return Menu[]
     */
    public function findLatest(){
        return $this->createQueryBuilder('m')
            ->setMaxResults(8)
            ->getQuery()
            ->getResult(Query::HYDRATE_SIMPLEOBJECT);
    }

    /**
     * @return Query
     */
    public function getAllMenusQuery(){
        return $this->createQueryBuilder('m')
            ->getQuery();
    }
}
