<?php
namespace AppBundle\Twig;


class AppExtension extends \Twig_Extension
{

    public function getFunctions(){
        return [
            new \Twig_SimpleFunction('getRandom', [$this,'getRandom']),
        ];
    }

    public function getRandom(){
        return rand(1,8);
    }
    public function getName()
    {
        return 'getRandom';
    }
}
