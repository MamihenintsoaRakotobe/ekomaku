<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use AppBundle\Entity\Menu;



class MenuController extends Controller {
    

    
    /**
     * @Route("/menus", name="menus.home")
     * @return Response
     */
    public function indexAction(Request $request){

        $paginator = $this->get('knp_paginator');
        $rep = $this->getDoctrine()->getRepository(Menu::class);
        $menus = $paginator->paginate(
            $rep->getAllMenusQuery(),
            $request->query->getInt('page', 1),
            8
        );
        
        return $this->render('menu/index.html.twig',[
            'menus' => $menus
        ]);
    }

    /**
     * @Route("/menus/{slug}-{id}", name="menus.show", requirements={"slug":"[a-z0-9\-]*"})
     * @return Response
     */
    public function show(Request $request,$slug,$id){

        $rep = $this->getDoctrine()->getRepository(Menu::class);
        $menu = $rep->find($id);
        if($menu->getSlug() !== $slug){
            return $this->redirectToRoute('menus.show',[
                'id' => $menu->getId(),
                'slug' => $menu->getSlug()
            ],301);
        }
        return $this->render('menu/show.html.twig',[
            'menu' => $menu
        ]);
    }
}