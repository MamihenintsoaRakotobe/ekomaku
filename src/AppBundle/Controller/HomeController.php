<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Faker\Factory;
use AppBundle\Entity\Menu;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class HomeController extends Controller{

    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function indexAction(Request $request){
        /*$faker = Factory::create("fr_FR");
        $em = $this->getDoctrine()->getManager();
        
         for($i = 0; $i<50;$i++){
             $menu = new Menu();
             $menu->setName($faker->name);
             $menu->setDescription($faker->paragraph());
             $menu->setPrice($faker->biasedNumberBetween(10,50));
             $em->persist($menu);
            
         }
         $em->flush();*/
        $authenticationUtils = $this->get('security.authentication_utils');
        $encoder = $this->get('security.password_encoder');
        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();
        $rep = $this->getDoctrine()->getRepository(User::class);

        $user = $rep->findDefaultUser();
        if(empty($user)){
            $em = $this->getDoctrine()->getManager();
            $newUser = new User();
            $newUser->setUsername('yuri');
            $newUser->setPassword($encoder->encodePassword($newUser,'yuri'));
            $em->persist($newUser);
            $em->flush();
        }
        return $this->render('default/index.html.twig',[
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }


    /*public function login(Request $request, AuthenticationUtils $authenticationUtils){
        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('default/index.html.twig',[
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }*/



}