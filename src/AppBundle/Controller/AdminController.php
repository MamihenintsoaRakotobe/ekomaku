<?php
namespace AppBundle\Controller;

use AppBundle\Form\MenuType;
use AppBundle\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Menu;


class AdminController extends Controller {

    /**
     * @Route("/admin",name="admin.menu.index")
     * @return Response|null
     */
    public function indexAction(Request $request){
        $paginator = $this->get('knp_paginator');
        $repository = $this->getDoctrine()->getRepository(Menu::class);
        $menus = $paginator->paginate(
            $repository->getAllMenusQuery(),
            $request->query->getInt('page', 1),
            8
        );
        return $this->render('admin/menu/index.html.twig',compact('menus'));
    }

    /**
     * @Route("/admin/menu/create",name="admin.menu.create")
     */
    public function newAction(Request $request){
        $menu = new Menu();
        $em = $this->getDoctrine()->getManager();
        $form =  $this->createForm(MenuType::class,$menu);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($menu);
            $em->flush();
            $this->addFlash('success','Menu crée avec succès');
            return $this->redirectToRoute('admin.menu.index');
        }
        return $this->render('admin/menu/new.html.twig',[
            'menu' => $menu,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/menu/{id}",name="admin.menu.edit",methods="GET|POST")
     * @return Response
     */
    public function editAction(Menu $menu,Request $request){
        $form =  $this->createForm(MenuType::class,$menu);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if($form->isSubmitted() && $form->isValid()){
            $em->flush();
            $this->addFlash('success','Menu modifié avec succès');
            return $this->redirectToRoute('admin.menu.index');
        }
        return $this->render('admin/menu/edit.html.twig',[
            'menu' => $menu,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/menu/{id}",name="admin.menu.delete",methods="DELETE")
     * @param Menu $menu
     * @param Request $request
     *
     */
    public function deleteAction(Menu $menu,Request $request){
            $em = $this->getDoctrine()->getManager();
            if($this->isCsrfTokenValid('delete'.$menu->getId(),$request->get('_token'))){
                    $em->remove($menu);
                    $em->flush();
                    $this->addFlash('success','Menu supprimé avec succès');
            }

            return $this->redirectToRoute('admin.menu.index');
    }
}